import React from 'react';
import Aux from '../HOC/Aux';

const Bars = () => {

  const bars_code = (

    <svg version="1.1" height="20px" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
      viewBox="0 0 24 21" style={{enableBackground:'new 0 0 24 21'}}>
      <style type="text/css">
        {`.bar{fill:#2D979D}`}
      </style>
      <path className="bar" d="M22.5,3h-21C0.7,3,0,2.3,0,1.5S0.7,0,1.5,0h21C23.3,0,24,0.7,24,1.5S23.3,3,22.5,3z"/>
      <path className="bar" d="M22.5,12h-21C0.7,12,0,11.3,0,10.5S0.7,9,1.5,9h21c0.8,0,1.5,0.7,1.5,1.5S23.3,12,22.5,12z"/>
      <path className="bar" d="M22.5,21h-21C0.7,21,0,20.3,0,19.5S0.7,18,1.5,18h21c0.8,0,1.5,0.7,1.5,1.5S23.3,21,22.5,21z"/>
    </svg>

  );

  return(
    <Aux>
      {bars_code}
    </Aux>
  )

};

export default Bars;  