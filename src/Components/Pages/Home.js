import React from 'react';
import Header from '../Header/Header';
import PostContainer from '../PostContainer/PostContainer';
import Footer from '../Footer/Footer';
import Aux from '../HOC/Aux';

function Home() {
  return (
      <Aux>
        <Header />
        <PostContainer />
        <Footer />
      </Aux>
  );
}

export default Home;