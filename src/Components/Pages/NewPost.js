import React, { Component } from 'react';
import Aux from '../HOC/Aux';
import NewHeader from '../NewHeader/NewHeader';
import NewForm from '../NewForm/NewForm';
import PostTypeSelector from '../PostTypeSelector/PostTypeSelector';
import axios from 'axios';

class NewPost extends Component {

  state = {
    newPostContent: {
      title: '',
      url: ''
    }
  }

  titleChangeHandler = (event) => {
    const oldUrl = this.state.newPostContent.url;

    this.setState({
      newPostContent: {
        title: event.target.value,
        url: oldUrl
      }
    })
  }

  urlChangeHandler = (event) => {
    const oldTitle = this.state.newPostContent.title;

    this.setState({
      newPostContent: {
        title: oldTitle,
        url: event.target.value
      }
    })
  }

  handlePost = () => {
    const post = {
      "id": 16,
      "type": "LINK",
      "title": "NewPost",
      "meta": {
        "url": "uuu-rrr-lll"
      }
    }

    axios.post('http://localhost:5050/posts', post)
    .then(response => {
      console.log(response);
    })
  }

  render() {
    return (
      <Aux>
        <NewHeader click={this.handlePost} />
        <NewForm titleChanged={this.titleChangeHandler} urlChanged={this.urlChangeHandler} />
        <PostTypeSelector />
      </Aux>
    );
  }
}

export default NewPost;