import React from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { Route } from 'react-router-dom';
import Home from './Components/Pages/Home';
import NewPost from './Components/Pages/NewPost';

function App() {
  return (
    <BrowserRouter>
        <Route path="/" exact component={Home} />
        <Route path="/new-post" component={NewPost} />
    </BrowserRouter>
  );
}

export default App;