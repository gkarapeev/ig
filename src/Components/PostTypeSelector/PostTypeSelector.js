import React from 'react';
import './PostTypeSelector.css';

const PostTypeSelector = () => {

  return (
    <div className="typeSelector">
      <div className="type active">
        LINK
      </div>
      <div className="type">
        IMAGE
      </div>
    </div>
  )

};

export default PostTypeSelector;