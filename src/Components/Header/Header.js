import React from 'react';
import './Header.css';
import Logo from './Logo';
import Bars from './Bars';

const appHeader = () => {

  return (
    <div className='Header'>
      <a href="/"><Logo /></a>
      <div className='menu-toggle'>
        <Bars />
      </div>
    </div>
  )
};

export default appHeader;