import React from 'react';
import './NewHeader.css';

const NewHeader = (props) => {

  return (
    <div className="NewHeader">
      <a href="/" ><div className="x">X</div></a>
      <span>Create Post</span>
      <div onClick={props.click}>POST</div>
    </div>
  )

};

export default NewHeader;