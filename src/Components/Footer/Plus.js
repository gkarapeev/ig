import React from 'react';
import Aux from '../HOC/Aux';

const Plus = () => {

  const plus_code = (
    <svg version="1.1" width="24px" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
      viewBox="0 0 24 24" style={{'enableBackground':'new 0 0 24 24'}}>
      <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="14.0575" y1="23.6684" x2="9.9425" y2="0.3316">
        <stop  offset="0" style={{'stopColor':'#3AB481'}}/>
        <stop  offset="1" style={{'stopColor':'#2D979D'}}/>
      </linearGradient>
      <style>
        {`.plus-shape{fill:url(#SVGID_1_)}`}
      </style>
      <path className="plus-shape" d="M22,10h-8V2c0-1.1-0.9-2-2-2s-2,0.9-2,2v8H2c-1.1,0-2,0.9-2,2s0.9,2,2,2h8v8c0,1.1,0.9,2,2,2s2-0.9,2-2v-8h8
      c1.1,0,2-0.9,2-2S23.1,10,22,10z"/>
    </svg>
  );

  return(
    <Aux>
      {plus_code}
    </Aux>
  )

};

export default Plus;