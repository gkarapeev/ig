import React, { Component } from 'react';
import './PostContainer.css';
import Post from '../Post/Post';
import axios from '../../../node_modules/axios';

class PostContainer extends Component {

  constructor() {
    super();

    this.state = {
      posts: [],
      lastPost: 0
    }
  }


  componentDidMount () {
    this.getPosts();
    window.addEventListener('scroll', this.onScroll)
  }

  getPosts () {
    axios.get('http://localhost:5050/posts', {
      transformResponse: [object => {
        let chunk = JSON.parse(object).slice(this.state.lastPost, this.state.lastPost + 3);
        return chunk;
      }]
    })

    .then(response => {
      let posts = this.state.posts.concat(response.data);
      this.setState({posts: posts, lastPost: this.state.lastPost + 3});
    });
  }
  
  onScroll = () => {
    if (window.innerHeight + window.scrollY >= (document.body.offsetHeight - 300)) {
      this.getPosts();
    }
  }

  render() {

    const posts = this.state.posts.map(post => {
      return <Post title={post.title} type={post.type} meta={post.meta} key={post.id}/>
    });

    return (
      <div className="post-container">
        {posts}
      </div>

      )
  }
};

export default PostContainer;