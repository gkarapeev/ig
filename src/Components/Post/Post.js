import React from 'react';
import './Post.css';
import Info from './Info';

const Post = (props) => {

  let body;

  switch (props.type) {
    case 'IMAGE':
      body = (<img alt="ok" src={props.meta.url} />);
      break;
    case 'VIDEO':
      const vidKey = props.meta.url.slice(32, 43);
      body = (<iframe title={props.title} width="100%" height="315" src={`https://www.youtube.com/embed/${vidKey}`} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>);
      break;
    case 'LINK':
      body = (<div style={{background: "#44ad74", height: "120px", display: "flex", alignItems: "center", padding: "2em"}}>{props.meta.url}</div>);
      break;
    default:
    break;
  }

  return (
    <div className="post-shell">
      <div className="post-body">
        {body}
      </div>
      <Info title={props.title} />
    </div>
  )
};

export default Post;