import React from 'react';
import './NewForm.css';

const NewForm = (props) => {

  return (
    <form className="newForm">
      <input type="text" onChange={props.titleChanged} placeholder="Title..."></input>
      <input type="text" onChange={props.urlChanged} placeholder="Paste your link here"></input>
    </form>
  )

};

export default NewForm;