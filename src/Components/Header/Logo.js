import React from 'react';
import Aux from '../HOC/Aux';

const logo = () => {

const logo_code = (  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
  viewBox="0 0 100 100">
  <linearGradient id="SVGID_LOGO" gradientUnits="userSpaceOnUse" x1="70" y1="100" x2="0" y2="0">
    <stop  offset="0" style={{'stopColor':'#3AB481'}}/>
    <stop  offset="1" style={{'stopColor':'#2D979D'}}/>
  </linearGradient>
  <g>
    <path className='logo-svg' d="M76.4,62.6c-7.1,6.1-15.2,8.2-24,6.4c-6.2-1.2-11.4-4.5-15.3-9.4c-7.9-9.9-7.4-23.9,0.2-33.3c4.9-6,11.2-9.3,18.9-9.9
      c7.6-0.6,14.3,2,20.1,7c0.2-0.1,0.2-0.1,0.3-0.2c0-0.3,0-1.4,0-2.6c0-2.3,1.9-4.2,4.2-4.2l0,0c2.3,0,4.2,1.9,4.2,4.2
      c0,9.2,0,31.4,0,44.6c0,19.1-15.5,34.7-34.7,34.7l0,0l0,0c-19.2,0-34.7-15.5-34.7-34.7c0-17.2,0-37,0-44.6c0-2.7,2.4-4.5,4.2-4.4
      c2.3,0,4.3,2,4.3,4.5c0,11.1,0,41.1,0.1,45c0.2,10.1,4.9,17.7,13.5,22.7c5,2.9,10.4,3.9,16.2,3c8.4-1.3,14.7-5.7,19-12.8
      c2.7-4.4,3.9-9.3,3.7-14.5C76.7,63.8,76.5,63.4,76.4,62.6z M39.4,43.3c0,10.1,8.2,18.2,18,18.5c10.6,0.3,19.1-8.3,19.2-18.5
      c0.1-10.4-8.7-18.5-18-18.7C48,24.4,39.3,33,39.4,43.3z" />
    <circle className='logo-svg' cx="20" cy="5.1" r="5.1"/>
  </g>
</svg>);

  return(
    <Aux>
      {logo_code}
    </Aux>
  )
};

export default logo;  