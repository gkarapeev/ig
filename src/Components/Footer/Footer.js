import React from 'react';
import Plus from './Plus';
import './Footer.css';

const Footer = (props) => {
  return(
    <nav id="footer">
      <div className="new-post-button">
        <a href="/new-post"><Plus /></a>
      </div>
    </nav>
  )
};

export default Footer;