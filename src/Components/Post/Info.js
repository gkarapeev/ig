import React from 'react';
import './Info.css';

const Info = (props) => {
  return (
    <div className="info">
      <div className="info-top">
        <div className="info-title">
          {props.title}
        </div>
        <div className="modify">
          {'...'}
        </div>
      </div>
      <div className="time-stamp">
        {'4 days ago'}
      </div>
    </div>
  )
};

export default Info;